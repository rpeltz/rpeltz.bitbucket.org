# Assignment Learning Objectives and Resources

**Follow README.md instructions in each of the 10 folders in this repo.  You will be graded on the index.html of each of the folders.**

## Learning objectives for each directory
1. Viewport tag
2. Variables in CSS
3. Media Queries
4. Tabular Data
5. Flex box layout
6. Grid layout
7. Google Fonts
8. CSS: target pseudo selector to create the hamburger navigation component
9. Masonry layout using CSS column
10. Media sharing: audio/video hosting, video and map embedding
11. Fixed Layout


document.addEventListener("DOMContentLoaded", function (e) {
  document.querySelector("#speech-form").addEventListener("submit", function (e) {
    e.preventDefault();
    // alert('event');
    let textEl = document.querySelector("input[name=text-to-speak]");
    // console.log("XXXX",   textEl.value);
    // responsiveVoice.speak(textEl.value);
    console.log(textEl);
    // http://numbersapi.com/random/year?json
    // fetch('http://numbersapi.com/random/year?json')
    // fetch(`http://numbersapi.com/${textEl.value}?json`)

    //   .then(function (response) {
    //     return response.json();
    //   })
    //   .then(function (myJson) {
    //     console.log(JSON.stringify(myJson));
    //     // responsiveVoice.speak(myJson.text)
    //   });
    // fetch('https://favqs.com/api/qotd')
    fetch(`https://favqs.com/api/quotes?filter=${textEl.value}`,
      {
        headers: {
          'Authorization': 'Token token="e9ca67701ffad6414399c9cd2e2cff72"'
        }
      })

      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {
        // console.log(JSON.stringify(myJson));
        // responsiveVoice.speak(myJson.quotes[0].body)
        const list = document.querySelector('#quote-list')
        for (let quote of myJson.quotes){
          let listItem = document.createElement('li')
          let div1 = document.createElement('div')
          div1.innerHTML = quote.author
          let div2 = document.createElement('div')
          div2.innerHTML = quote.body
          let btn = document.createElement('button')
          btn.innerHTML = "speak"
          btn.addEventListener('click',event =>{
            responsiveVoice.speak(`From ${quote.author}`)
            responsiveVoice.speak(`${quote.body}`)
          })
          listItem.append(div1)
          listItem.append(div2)
          listItem.append(btn)
          list.appendChild(listItem)
          // document.writeln(quote.author)
          // document.writeln(quote.body)
        }
     
      });
  })

})